<?php
if (!function_exists('dump')) {
    /**
     * @param $arr 变量
     * @return 打印变量
     */
    function dump($arr)
    {
        echo '<pre>' . print_r($arr, TRUE) . '</pre>';
    }

}

if (!function_exists('curl')) {
    /**
     * @param $url 地址
     * @param  $data 要提交数组
     * @return Curl获取
     */
    function curl($url, $data = '')
    {
        $ch = curl_init();
        if (class_exists('\CURLFile')) {
            curl_setopt($ch, CURLOPT_SAFE_UPLOAD, true);
        } else {
            if (defined('CURLOPT_SAFE_UPLOAD')) {
                curl_setopt($ch, CURLOPT_SAFE_UPLOAD, false);
            }
        }
        preg_match('/https:\/\//', $url) ? $ssl = TRUE : $ssl = FALSE;
        if ($ssl) {
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        }
        curl_setopt($ch, CURLOPT_URL, $url);
        if (!empty($data)) {
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $d = curl_exec($ch);
        curl_close($ch);
        return $d;
    }
}
header('Content-type: application/json');
header('Access-Control-Allow-Origin: *');
$a = file_get_contents("php://input");
parse_str($a, $t);
echo curl("http://www.golangs.cn/api/sqlToStruct","sql=".$t['sql']."&pak_name=".$t["pak_name"]);